A Simple python script to start the ec2 instance
-----------------------------------------------

**Reqirements**

   * python 2.6+
   * python boto 2.38+
  


folder structure

```
- main.py
- ec2.list
```

ec2.list
---------------------
File contains the details of the instance to start up
format JSON

	{"name":"<name>","access_key":"<aws_key>","security_key","<security_key>","instanceid":["<id1>","<id2>"]}

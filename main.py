#!/usr/bin/python
import boto.ec2
import json

FILENAME = "ec2.list"
REGION = "ap-southeast-1"
success = 0
fail = 0

def readfile(fname):
   with open(fname) as f:
    content = f.readlines()
   return content

def process(content):
   json_data = json.loads(content)
   access_key = json_data['access_key']
   security_key = json_data['security_key']
   # create connection
   print "Starting instances ["+json_data['name']+"]"
   print "------------------------------------------"
   try:
      conn = boto.ec2.connect_to_region(REGION,aws_access_key_id=access_key,aws_secret_access_key=security_key)
      conn.start_instances(instance_ids=json_data['instanceid'])
      print "Instance "+ json_data['name']+" -OK"
      success = success + 1;
   except Exception, e1:
        error1 = "Error1: %s" % str(e1)
        print "Instance"+ json_data['name']+" -NOT OK"+ error1
        fail = fail + 1; 
   print "-------------------------------------------"

def status():
   print "******Completed**********"
   print "Ok -"+ str(success)
   print "NOT OK -"+ str(fail)

def main():
   print "*******************************"
   print "*      EC2 Startup service    *"
   print "*******************************"
   contents = readfile(FILENAME)
   for item in contents:
	process(item)
   status()   
   
if __name__ == "__main__":
   main()
